package repository;

import java.util.List;

public class MainHomework {
    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFileImpl("users.txt");
        List<User> usersAge = usersRepository.findByAge(25);


        for (User user : usersAge) {
            System.out.println("метод findByAge - " + user.getName () + " " + user.getAge () + " " + user.isWorker());
        }

        List<User> userIsWorker = usersRepository.findByIsWorkerIsTrue (false);


        for (User user : userIsWorker) {
            System.out.println("метод findByIsWorkerIsTrue - " + user.getName () + " " + user.getAge () + " " + user.isWorker());
        }

    }
}
