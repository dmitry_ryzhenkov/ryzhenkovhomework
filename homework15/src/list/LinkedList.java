package list;
public class LinkedList<T> {

    private static class Node<T> {
        T value;
        Node<T> next;

        public Node(T value) {
            this.value = value;
        }

        public T getValue() {
            return value;
        }
    }

    private Node<T> first;
    private Node<T> last;
    private Node<T> indexT; //Homework 15
    private int size;

    public void add(T element) {
        Node<T> newNode = new Node<>(element);
        if (size == 0) {
            first = newNode;
        } else {
            last.next = newNode;
        }
        last = newNode;
        size++;
    }

    public void addToBegin(T element) {
        Node<T> newNode = new Node<>(element);

        if (size == 0) {
            last = newNode;
        } else {
            newNode.next = first;
        }
        first = newNode;
        size++;
    }

    public int size() {
        return size;


    }
//Homework 15
    public T get(int index) {
        if (index > size) return null;

        int i = 0;
        indexT = first;
        while (index != i ){

            indexT = indexT.next;
            i++;
        }
        return indexT.getValue() ;
    }

}
