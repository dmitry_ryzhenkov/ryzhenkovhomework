package oop1;

import static oop1.UsersRepositoryFileImpl.users;

public class Main {
    public static void main(String[] args) {
// при написании методов следовал логике задания: вызвать объект, в объекте заменить имя и возраст, перезаписать файл
        User user = (UsersRepositoryFileImpl.findById(2));
        System.out.println(user.getName()); // проверка на корректность работы метода findById (в фале под id 2 - Виктор)

        user.setName("Марсель");
        user.setAge(27);

        UsersRepositoryFileImpl.findById(2); //меняем в id 2 Виктора на Марселя


        UsersRepositoryFileImpl.update(user);
        System.out.println("Имя обновленного юзера " + users.get(1).getName());


    }
}
