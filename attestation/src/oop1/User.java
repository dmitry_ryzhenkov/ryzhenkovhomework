package oop1;

public class User {
    private int id;
    private String name;
    private int age;
    private boolean isWorked;

    public User(int id, String name, int age, boolean isWorked) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.isWorked = isWorked;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isWorked() {
        return isWorked;
    }

    public void setWorked(boolean worked) {
        isWorked = worked;
    }
}
