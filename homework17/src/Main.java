import java.util.*;

public class Main {
    public static void main(String[] args) {

        String input = "абстрактный класс шаблон класс абстрактный абстрактный класс класс";

        String [] inputArr = input.split("\\s");

        Map<String, Integer> inputMap = new HashMap<>();

        for (String word : inputArr) {
            if (!inputMap.containsKey(word))
                inputMap.put(word, 1);
            else {
                int j = inputMap.get(word);
                j++;
                inputMap.put(word, j);

            }
        }
        Set<Map.Entry <String,Integer>> entries = inputMap.entrySet();
        for (Map.Entry<String, Integer> entry : entries){
           System.out.println("Слово: " + entry.getKey() + " - " + entry.getValue() );

        }
        //System.out.println(inputMap.toString());
    }
}
