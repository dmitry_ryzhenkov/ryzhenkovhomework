import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;
import java.util.Scanner;

public class MainHomework {
    public static void main(String[] args) {

    try (BufferedReader bufferedReader = new BufferedReader(new FileReader("auto.txt"))) {
            bufferedReader.lines()
                    .map(line -> line.split("\\|"))
                    .filter(filter ->  filter[2].equals("Black") | Integer.parseInt(filter[4]) == 0)
                    .forEach(number -> System.out.println(number[0]));
            //Номера всех автомобилей, имеющих черный цвет или нулевой пробег.
        } catch (IOException e) {
            throw new IllegalArgumentException();
        }

        //для тренировки добавил запрос диапазона цен с консоли.
        Scanner scanner = new Scanner(System.in);
        System.out.println("Минимальный диапазон цены");
        int minimum = scanner.nextInt();
        System.out.println("Максимальный диапазон цены");
        int maximum = scanner.nextInt();

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("auto.txt"))) {
            int price = (int) bufferedReader.lines().distinct()
                    .map(line -> line.split("\\|"))
                    .filter(filter -> Integer.parseInt(filter[4]) > minimum & Integer.parseInt(filter[4]) < maximum)
                    .count();
            System.out.println("Число авто в диапазоне цен = " + price);
            //Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс.
        } catch (IOException e) {
            throw new IllegalArgumentException();
        }


        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("auto.txt"))) {
            String colorOfAuto = bufferedReader.lines()
                    .map(line -> line.split("\\|"))
                    .min(Comparator.comparingInt(price -> Integer.parseInt(price[4])))
                    .map(color -> color[2]).orElse("Нет данных");
                    System.out.println("Цвет автомобиля с минимальной стоимостью - " + colorOfAuto);
            //Вывести цвет автомобиля с минимальной стоимостью.
        } catch (IOException e) {
            throw new IllegalArgumentException();
        }


    }
}