public class Shifter implements MoveFigure{

    double shiftX;
    double shiftY;

    public Shifter(double shiftX, double shiftY) {
        this.shiftX = shiftX;
        this.shiftY = shiftY;
    }

    public void setShiftX(double shiftX) {
        this.shiftX = shiftX;
    }

    public void setShiftY(double shiftY) {
        this.shiftY = shiftY;
    }

    @Override
    public double newX() {
        return shiftX;
    }

    @Override
    public double newY() {
        return shiftY;
    }
}
