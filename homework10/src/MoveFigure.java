public interface MoveFigure {
    double newX();
    double newY();
}
