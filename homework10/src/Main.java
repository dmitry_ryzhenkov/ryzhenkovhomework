public class Main {
    public static void main(String[] args) {

        Ellipse ellipse = new Ellipse(2, 3, 2, 5);
        Circle circle = new Circle(4, 4.6, 10.2);
        Rectangle rectangle = new Rectangle(1, 2.2, 3.3, 3.9);
        Square square = new Square(2, 5.2, 6.2);

        System.out.println("circle X - " + circle.getCorrX() + " Y - " + circle.getCorrY());
        System.out.println("square X - " + square.getCorrX() + " Y - " + square.getCorrY());
        Shifter shifter1 = new Shifter(5, 5);

        Figure[] shiftFigures = new Figure[3];
        shiftFigures[0] = circle;
        shiftFigures[1] = square;
        shiftFigures[2] = rectangle;

        for (Figure shiftFigure : shiftFigures) {
            shiftFigure.setMoveFigure(shifter1);
            shiftFigure.Move();
        }

        System.out.println("circle newX - " + circle.getCorrX() + " newY - " + circle.getCorrY());
        System.out.println("square newX - " + square.getCorrX() + " newY - " + square.getCorrY());
        System.out.println("rect newX - " + rectangle.getCorrX() + " newY - " + rectangle.getCorrY());
    }


}
