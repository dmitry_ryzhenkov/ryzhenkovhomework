public abstract class Figure {

    protected double corrX;
    protected double corrY;

    protected Figure(double corrX, double corrY) {
        this.corrX = corrX;
        this.corrY = corrY;
    }

    protected MoveFigure moveFigure;

    public void setMoveFigure(MoveFigure moveFigure) {
        this.moveFigure = moveFigure;
    }

    public void Move() {
        corrX = moveFigure.newX();
        corrY = moveFigure.newY();
    }

    abstract double cetPerimeter();

    public double getCorrX() {

        return corrX;
    }

    public double getCorrY() {

        return corrY;
    }

    public void setCorrX(double corrX) {

        this.corrX = corrX;
    }

    public void setCorrY(double corrY) {

        this.corrY = corrY;
    }
}
