public class Rectangle extends Figure {

    protected Rectangle(double corrX, double corrY, double sideLong, double sideShort) {
        super(corrX, corrY);
        this.sideLong = sideLong;
        this.sideShort = sideShort;
    }
    public void Move() {
    }

    private double sideLong;
    private double sideShort;


    public double cetPerimeter() {
        return (sideLong + sideShort) * 2;
    }

    public double getSideLong() {
        return sideLong;
    }

    public double getSideShort() {
        return sideShort;
    }

    public void setSideLong(double sideLong) {
        this.sideLong = sideLong;
    }

    public void setSideShort(double sideShort) {
        this.sideShort = sideShort;
    }
}


