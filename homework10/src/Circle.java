public class Circle extends Ellipse {

    protected Circle(double corrX, double corrY, double diameter) {
        super(corrX, corrY, diameter, diameter);
    }

    public void Move() {
        corrX = moveFigure.newX();
        corrY = moveFigure.newY();
    }
    public double cetPerimeter() {

        return 3.14 * diamMajor;
    }

}

