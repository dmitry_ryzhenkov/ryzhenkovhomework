public class Ellipse extends Figure {

    protected double diamMajor;
    private double diamMinor;

    protected Ellipse(double corrX, double corrY, double diamMajor, double diamMinor) {
        super(corrX, corrY);
        this.diamMajor = diamMajor;
        this.diamMinor = diamMinor;
    }

    public void Move() {
    }

    public Ellipse(double corrX, double corrY, double diamMajor) {
        super(corrX, corrY);
    }

    public double getDiamMinor() {
        return diamMinor;
    }

    public double getDiamMajor() {
        return diamMajor;
    }

    public double cetPerimeter() {
        return 4 * ((3.14 * diamMajor * diamMinor + (diamMajor - diamMinor))) / (diamMinor + diamMajor);
    }


}


